#include <KarnageServerPCH.h>

PickupServer::PickupServer() {}

void PickupServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject(this);
}

bool PickupServer::HandleCollisionWithPlayer(Player* inPlayer)
{
	if(!picked)
		ApplyPickup(inPlayer);

	SetDoesWantToDie(true);

	return false;
}

bool PickupServer::HandleCollisionWithGameObject(GameObject* inObj)
{
	/* Handle Collision Response */
	return false;
}

#pragma region Apply Pickup

void PickupServer::ApplyPickup(Player* inPlayer)
{
	switch (mPickUpType)
	{
	case Enum::PickupType::HEALTH:
		ApplyHealthPickup(inPlayer);
		break;
	case Enum::PickupType::ARMOUR:
		ApplyArmourPickup(inPlayer);
		break;
	case Enum::PickupType::CLOAK:
		ApplyCloakPickup(inPlayer);
		break;
	case Enum::PickupType::AMMO:
		ApplyAmmoPickup(inPlayer);
		break;
	case Enum::PickupType::FIRE_RATE:
		ApplyFireRatePickup(inPlayer);
		break;
	case Enum::PickupType::SHOTGUN:
		ApplyShotgunPickup(inPlayer);
		break;
	default:
		break;
	}
}

void PickupServer::ApplyHealthPickup(Player* inPlayer)
{
	inPlayer->PickupHealth(Constant::Pickup::PICKUP_HEALTH);
	NetworkManagerServer::sInstance->SetStateDirty(inPlayer->GetNetworkId(), PlayerReplication::State::Health);
}

void PickupServer::ApplyArmourPickup(Player* inPlayer)
{
	inPlayer->PickupArmour(Constant::Pickup::PICKUP_ARMOUR);
	NetworkManagerServer::sInstance->SetStateDirty(inPlayer->GetNetworkId(), PlayerReplication::State::Armour);
}

void PickupServer::ApplyCloakPickup(Player* inPlayer)
{
	inPlayer->SetPickupType(Enum::PickupType::CLOAK);
	NetworkManagerServer::sInstance->SetStateDirty(inPlayer->GetNetworkId(), PlayerReplication::State::Pickup);
}

void PickupServer::ApplyAmmoPickup(Player* inPlayer)
{
	inPlayer->SetPickupType(Enum::PickupType::AMMO);
	NetworkManagerServer::sInstance->SetStateDirty(inPlayer->GetNetworkId(), PlayerReplication::State::Pickup);
}

void PickupServer::ApplyFireRatePickup(Player* inPlayer)
{
	inPlayer->SetPickupType(Enum::PickupType::FIRE_RATE);
	NetworkManagerServer::sInstance->SetStateDirty(inPlayer->GetNetworkId(), PlayerReplication::State::Pickup);
}
void PickupServer::ApplyShotgunPickup(Player* inPlayer)
{
	inPlayer->SetPickupType(Enum::PickupType::SHOTGUN);
	NetworkManagerServer::sInstance->SetStateDirty(inPlayer->GetNetworkId(), PlayerReplication::State::Pickup);
}
#pragma endregion