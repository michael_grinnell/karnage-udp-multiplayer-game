#include <KarnageServerPCH.h>

namespace
{
	const float kRespawnDelay = Constant::Player::RESPAWN_DELAY;
}

ClientProxy::ClientProxy(const SocketAddress& inSocketAddress, const string& inName, int inPlayerId) :
                                                                                                     mDeliveryNotificationManager(
	                                                                                                     false, true)
                                                                                                     , mSocketAddress(
	                                                                                                     inSocketAddress)
                                                                                                     , mName(inName)
                                                                                                     , mPlayerId(
	                                                                                                     inPlayerId)
                                                                                                     , mTimeToRespawn(
	                                                                                                     0.f)
                                                                                                     , mIsLastMoveTimestampDirty(
	                                                                                                     false)
{
	UpdateLastPacketTime();
}

void ClientProxy::UpdateLastPacketTime()
{
	mLastPacketFromClientTime = Timing::sInstance.GetTimef();
}

void ClientProxy::HandlePlayerDied()
{
	mTimeToRespawn = Timing::sInstance.GetFrameStartTime() + kRespawnDelay;
}

void ClientProxy::RespawnPlayerIfNecessary()
{
	if (mTimeToRespawn != 0.f && Timing::sInstance.GetFrameStartTime() > mTimeToRespawn)
	{
		static_cast<Server*> (Engine::sInstance.get())->SpawnPlayerForPlayer(GetPlayerId(), GetName());
		mTimeToRespawn = 0.f;
	}
}
