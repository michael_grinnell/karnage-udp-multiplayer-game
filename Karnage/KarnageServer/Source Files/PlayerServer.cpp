#include <KarnageServerPCH.h>

PlayerServer::PlayerServer() :
                             mWorldBounds(sf::FloatRect(0, 0, 1536, 1408)) //TODO Get Value From TileManager
                             , mPlayerControlType(ESCT_Human)
                             , mTimeOfNextShot(0.f)
                             , mTimeOfNextLavaDamage(0.f)
                             , mTimeBetweenLavaDamage(0.4f)
                             , oldRotation(0)
                             , mPickupTimer(Constant::Pickup::PICKUP_TIMER)
                             , mDeathAnimationTimer(1.f)
                             , mPickupTimerStarted(false)
                             , mReadyToThrow(false)
                             , mThrowGrenade(false)
                             , MGrenadeTimer(10.f) {}

#pragma region Update

void PlayerServer::Update()
{
	SetOldLocation();
	UpdateMoves();
	HandleEvents();
	UpdateLocation();
}

void PlayerServer::SetOldLocation()
{
	oldLocation = GetLocation();
	oldVelocity = GetVelocity();
	oldRotation = GetRotation();
}

void PlayerServer::UpdateMoves()
{
	if (mPlayerControlType == ESCT_Human)
	{
		ClientProxyPtr client
			= NetworkManagerServer::sInstance->GetClientProxy(GetPlayerId());

		if (client && !mDeathAnimationTimer.IsStarted())
			ProcessMoveList(client);
	}
	else
		SimulateMovement(Timing::sInstance.GetDeltaTime()); 	

}

void PlayerServer::UpdateLocation() const
{
	if (oldLocation != GetLocation() 
		|| oldVelocity != GetVelocity()
		|| oldRotation != GetRotation())
	{
		NetworkManagerServer::sInstance->SetStateDirty(GetNetworkId(), PlayerReplication::State::Pos);
	}
}

void PlayerServer::ProcessMoveList(ClientProxyPtr client)
{
	MoveList& moveList = client->GetUnprocessedMoveList();

	for (const Move& unprocessedMove : moveList)
	{
		const InputState& currentState = unprocessedMove.GetInputState();
		float deltaTime = unprocessedMove.GetDeltaTime();
		ProcessInput(deltaTime, currentState);
		SimulateMovement(deltaTime);
	}
	moveList.Clear();
}

#pragma endregion

#pragma region Player Events

void PlayerServer::TakeDamage(uint8_t damageAmount, uint32_t playerId)
{
	if (mArmour > 0)
	{
		if (mArmour - damageAmount <= 0)
		{
			mArmour = 0;
		}
		else
		{
			mArmour -= damageAmount;
		}
		NetworkManagerServer::sInstance->SetStateDirty(GetNetworkId(), PlayerReplication::State::Armour);
	}
	else
	{
		if (mHealth - damageAmount <= 0)
		{
			mHealth = 0;
			CreateBloodstain();
			//Quick Fix for lava damage
			if (playerId != -1)
				IncScore(playerId);

			mDeathAnimationTimer.Start();
		}
		else {
			mHealth -= damageAmount;
		}

		NetworkManagerServer::sInstance->SetStateDirty(GetNetworkId(), PlayerReplication::State::Health);
	}
		
}

void PlayerServer::IncScore(uint32_t playerId)
{
	ScoreBoardManager::sInstance->IncScore(playerId, 1);
}

void PlayerServer::KillPlayer(uint32_t playerID)
{
	SetDoesWantToDie(true);
	ClientProxyPtr clientProxy = NetworkManagerServer::sInstance->GetClientProxy(playerID);
	if (clientProxy)
		clientProxy->HandlePlayerDied();
}

#pragma endregion

#pragma region Handle Events

void PlayerServer::HandleEvents()
{
	HandleLavaCollision();
	HandlePickups();
	HandleShooting();
	HandleGrenades();
	HandleReadyToDie();
}

void PlayerServer::CreateBloodstain()
{
	shared_ptr<Bloodstain> stain =
		std::static_pointer_cast<Bloodstain>(GameObjectRegistry::sInstance->CreateGameObject('BLUD'));
	stain->SetLocation(this->GetLocation());
	stain->SetRotation(this->getRotation());
	stain->SetBloodstainType(Utility::randomInt(Enum::BloodstainType::BLOODSTAIN_COUNT));
}

void PlayerServer::HandleLavaCollision()
{
	if (!mWorldBounds.contains(GetLocation()))
	{
		float time = Timing::sInstance.GetFrameStartTime();
		if (time > mTimeOfNextLavaDamage && mHealth!=0)
		{
			mTimeOfNextLavaDamage = time + mTimeBetweenLavaDamage;
			TakeDamage(Constant::World::LAVA_DAMAGE, -1);
		}
	}
}

void PlayerServer::HandleShooting()
{
	float time = Timing::sInstance.GetFrameStartTime();
	if (mIsShooting && Timing::sInstance.GetFrameStartTime() > mTimeOfNextShot)
	{
		mTimeOfNextShot = time + mTimeBetweenShots; //fire!
		if (this->GetShotgun())
		{
			for (int i = 0; i < 3; ++i)
			{
				BulletPtr bullet = std::static_pointer_cast<Bullet>(GameObjectRegistry::sInstance->CreateGameObject('BULT'));
				bullet->InitFromShooter(this, Utility::randomRange(20 + i));
			}
		}
		else {
			BulletPtr bullet = std::static_pointer_cast<Bullet>(GameObjectRegistry::sInstance->CreateGameObject('BULT'));
			bullet->InitFromShooter(this, Utility::randomRange(20));
		}
		ApplyKickBack(Timing::sInstance.GetFrameStartTime());
	}

	if (mThrowGrenade && Timing::sInstance.GetFrameStartTime() > mTimeOfNextShot)
	{
		mThrowGrenade = false;
		--mGrenadeAmmo;
		float throwSpeed = (MGrenadeTimer.GetElapsedSeconds() / 2) + Constant::World::GRENADE_THROW_SPEED;
		
		Utility::clamp(throwSpeed, 0, Constant::World::GRENADE_MAX_DISTANCE);

		mTimeOfNextShot = time + mTimeBetweenShots;

		GrenadePtr grenade = std::static_pointer_cast<Grenade>(GameObjectRegistry::sInstance->CreateGameObject('GRDE'));
		grenade->SetThrowSpeed(throwSpeed);
		grenade->InitFromPlayer(this, Utility::randomRange(20));
		MGrenadeTimer.Reset();

		NetworkManagerServer::sInstance->SetStateDirty(GetNetworkId(), PlayerReplication::State::Ammo);
	}
}

void PlayerServer::HandleGrenades()
{
	//TODO REFACTOR THIS
	if ((mGrenadeAmmo) > 0)
	{
		if (mIsThrowingGrenade)
		{

			if (!MGrenadeTimer.IsStarted())
			{
				MGrenadeTimer.Start();
			}

			mReadyToThrow = true;
			MGrenadeTimer.Update(Timing::sInstance.GetDeltaTime());
		}

		if (mReadyToThrow && !mIsThrowingGrenade)
		{
			mReadyToThrow = false;
			mThrowGrenade = true;
		}
	}

}

void PlayerServer::HandlePickups()
{
	if (GetPickupType() != Enum::PickupType::NONE)
		HandlePickupTimer();
}

void PlayerServer::HandleReadyToDie()
{
	if (mDeathAnimationTimer.IsStarted())
		mDeathAnimationTimer.Update(Timing::sInstance.GetDeltaTime());
	
	if (mDeathAnimationTimer.IsFinished())
		KillPlayer(GetPlayerId());
}

void PlayerServer::HandlePickupTimer()
{
	if (mPickupTimer.IsStarted())
		UpdatePickupTimer();
	else
		StartPickupTimer();
}

void PlayerServer::StartPickupTimer()
{
	mPickupTimer.Start();
	ApplyPickup();
}

void PlayerServer::UpdatePickupTimer()
{
	mPickupTimer.Update(Timing::sInstance.GetDeltaTime());

	if (mPickupTimer.IsFinished())
	{
		mPickupTimer.Reset();
		mPickupType = Enum::PickupType::NONE;
		NetworkManagerServer::sInstance->SetStateDirty(GetNetworkId(), PlayerReplication::State::Pickup);
		ApplyPickup();
	}
}

void PlayerServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject(this);
}

#pragma endregion