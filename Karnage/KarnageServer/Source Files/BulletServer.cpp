#include <KarnageServerPCH.h>

BulletServer::BulletServer() :
	mTimeToDie(Timing::sInstance.GetFrameStartTime() + Constant::World::BULLET_LIFETIME)
{
}

void BulletServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject(this);
}

void BulletServer::Update()
{
	Bullet::Update();

	if(Timing::sInstance.GetFrameStartTime() > mTimeToDie)
	{
		SetDoesWantToDie(true);
	}
}

bool BulletServer::HandleCollisionWithPlayer(Player* inPlayer)
{
	if(inPlayer->GetPlayerId() != GetPlayerId())
	{
		SetDoesWantToDie(true);
		dynamic_cast<PlayerServer*>(inPlayer)->TakeDamage(Constant::World::BULLET_DAMAGE, GetPlayerId());
	}

	return false;
}

bool BulletServer::HandleCollisionWithGameObject(GameObject* inObj)
{
	/* Handle Collision Response */
	if (inObj->GetObjectID() != GetObjectID())
	{
		switch (inObj->GetObjectID())
		{
		case 'OBST':
			SetDoesWantToDie(true);
			break;
		default:
			break;
		}
	}

	return false;
}