#include <KarnageServerPCH.h>

GrenadeServer::GrenadeServer() :
	 mGrenadeTimer(Constant::World::GRENADE_LIFETIME)
	, mExplosionTimer(Constant::Animation::EXPLOSION_DURATION)
{
}

void GrenadeServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject(this);
}

void GrenadeServer::Update()
{
	SetOldLocation();
	HandleLocation();



	if (!mGrenadeTimer.IsFinished())
	{
		if (!mGrenadeTimer.IsStarted())
		{
			mGrenadeTimer.Start();
		}
		else
		{
			mGrenadeTimer.Update(Timing::sInstance.GetDeltaTime());
		}
		
	}

	if (mGrenadeTimer.IsFinished() && !mExplosionTimer.IsStarted())
	{
		mIsExploding = true;
		NetworkManagerServer::sInstance->SetStateDirty(GetNetworkId(), GrenadeReplication::State::Exploding);
		mExplosionTimer.Start();
		SetCollisionRadius(Constant::World::EXPLOSION_RADIUS);
	}
	

	if (mExplosionTimer.IsStarted())
	{
		mExplosionTimer.Update(Timing::sInstance.GetDeltaTime());
	}

	if (mExplosionTimer.IsFinished())
	{
		SetDoesWantToDie(true);
	}


	if (oldLocation != GetLocation()
		|| oldVelocity != GetVelocity()
		|| oldRotation != GetRotation())
	{
		NetworkManagerServer::sInstance->SetStateDirty(GetNetworkId(), GrenadeReplication::State::Pos);
	}
}

void GrenadeServer::SetOldLocation()
{
	oldLocation = GetLocation();
	oldVelocity = GetVelocity();
	oldRotation = GetRotation();
}

bool GrenadeServer::HandleCollisionWithPlayer(Player* inPlayer)
{
	if (inPlayer->GetPlayerId() != GetPlayerId())
	{
		if(mExplosionTimer.IsStarted())
		{

		//	LOG(" distance between grenade and player: %d", Utility::NormalizedEuclideanDistance(this->GetLocation(), inPlayer->GetLocation()));
			dynamic_cast<PlayerServer*>(inPlayer)->TakeDamage(Constant::World::GRENADE_DAMAGE, GetPlayerId());
		}
		else
			SetVelocity(sf::Vector2f(-GetVelocity().x / 2, -GetVelocity().y / 2));
	}
	else if (mExplosionTimer.IsStarted())
	{
		LOG(" distance between grenade and player: %d", Utility::NormalizedEuclideanDistance(this->GetLocation(), inPlayer->GetLocation()));
		dynamic_cast<PlayerServer*>(inPlayer)->TakeDamage(Constant::World::GRENADE_DAMAGE, GetPlayerId());
	}
	
	

	return false;
}

bool GrenadeServer::HandleCollisionWithGameObject(GameObject* inObj)
{
	/* Handle Collision Response */
	return false;
}