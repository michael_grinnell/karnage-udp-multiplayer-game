#pragma once
class BulletServer : public Bullet{
public:
	static GameObjectPtr StaticCreate()
	{
		return NetworkManagerServer::sInstance->RegisterAndReturn(new BulletServer());
	}

	void HandleDying() override;
	bool HandleCollisionWithPlayer(Player* inPlayer) override;
	bool HandleCollisionWithGameObject(GameObject* inObj) override;
	void Update() override;
protected:
	BulletServer();
private:
	float mTimeToDie;
};
