#pragma once
class GrenadeServer : public Grenade {
public:
	static GameObjectPtr StaticCreate()
	{
		return NetworkManagerServer::sInstance->RegisterAndReturn(new GrenadeServer());
	}

	void HandleDying() override;
	bool HandleCollisionWithPlayer(Player* inPlayer) override;
	bool HandleCollisionWithGameObject(GameObject* inObj) override;
	void Update() override;
	
protected:
	GrenadeServer();

private:
	void SetOldLocation();

private:
	float mTimeToDie;
	float oldRotation;

	Timer mGrenadeTimer;
	Timer mExplosionTimer;

	sf::Vector2f oldLocation;
	sf::Vector2f oldVelocity;
};
