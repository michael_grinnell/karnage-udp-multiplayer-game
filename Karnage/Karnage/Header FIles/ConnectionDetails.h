
class ConnectionDetails{
public:
	static void StaticInit();
	static std::unique_ptr<ConnectionDetails> sInstance;
	ConnectionDetails();
	void Load();
	string GetClientName() const;
	string GetClientDestination() const;
	uint16_t GetServerPort() const;
private:
	string m_clientName;
	string m_clientIP;
	uint16_t m_clientPort;
	uint16_t m_serverPort;
};
