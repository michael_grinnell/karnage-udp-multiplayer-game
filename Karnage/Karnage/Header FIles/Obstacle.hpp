#pragma
class Obstacle : public GameObject {
public:
	CLASS_IDENTIFICATION('OBST', GameObject)


	static GameObject* StaticCreate()
	{
		return new Obstacle();
	}

	uint32_t GetAllStateMask() const override
	{
		return  ObstacleReplication::State::AllState;
	}

	uint32_t Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const override;
	void WriteObstaclePosition(OutputMemoryBitStream& inOutputStream, uint32_t& writtenState, uint32_t& inDirtyState) const;
	void WriteObstacleType(OutputMemoryBitStream& inOutputStream, uint32_t& writtenState, uint32_t& inDirtyState) const;

	bool HandleCollisionWithPlayer(Player* inPlayer) override;
	bool HandleCollisionWithGameObject(GameObject* inObj) override;

	void SetObstacleType(uint8_t type);

	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

protected:
	Obstacle();
	Enum::ObstacleType mObstacleType;
};
