class InputState{
public:
	InputState() :
	             mDesiredRightAmount(0)
	             , mDesiredLeftAmount(0)
	             , mDesiredForwardAmount(0)
	             , mDesiredBackAmount(0)
	             , mIsShooting(false)
				 , mIsThrowingGrenade(false)
	             , mRotation(0) { }

	float GetDesiredHorizontalDelta() const
	{
		return mDesiredRightAmount - mDesiredLeftAmount;
	}

	float GetDesiredVerticalDelta() const
	{
		return mDesiredForwardAmount - mDesiredBackAmount;
	}

	bool IsShooting() const
	{
		return mIsShooting;
	}

	bool IsThrowingGrenade() const
	{
		return mIsThrowingGrenade;
	}

	bool Write(OutputMemoryBitStream& inOutputStream) const;

	bool Read(InputMemoryBitStream& inInputStream);

	int GetRotation() const
	{
		return mRotation;
	}

private:
	friend class InputManager;
	float mDesiredRightAmount, mDesiredLeftAmount;
	float mDesiredForwardAmount, mDesiredBackAmount;
	bool mIsShooting;
	bool mIsThrowingGrenade;

	uint16_t mRotation;
};
