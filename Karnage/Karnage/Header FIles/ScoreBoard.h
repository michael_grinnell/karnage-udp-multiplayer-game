#pragma once
class ScoreBoard : public sf::Transformable, public sf::Drawable
{
public:
	ScoreBoard();
	void UpdateScoreBoard();
	void SetScoreBoardInfo();

	void DrawScoreBoard(sf::RenderTarget& target, sf::RenderStates states) const;

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

private: 
	sf::Text scoreBoard;
	sf::Font zombieFont;
};