#pragma once

class Powerbar : public sf::Transformable, public sf::Drawable
{
public:
	Powerbar(sf::Vector2f dimensions);
	void setColor(sf::Color color);
	void setOutlineThickness(float thickness);

	void setPosition(sf::Vector2f pos);
	void setPower(int power);

	sf::Vector2f GetDimensions() const;

	float GetOutlineThinkness() const;

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

private:
	//virtual void drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;

private:
	sf::RectangleShape mFill;
	sf::RectangleShape mOutline;

	sf::Vector2f mDimensions;
};
