#pragma once

class PlayerArrow : public sf::Transformable, public sf::Drawable
{
public:
	PlayerArrow();
	void SetText();
	void setColor(sf::Color color);

	void setPosition(sf::Vector2f pos);
	void setName(string name);

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

private:
	sf::CircleShape mArrow;

	sf::Font zombieFont;
	sf::Text mPlayerNameText;

	string mPlayerName;
};
