class SoundManager{
public:
	static void StaticInit();
	static std::unique_ptr<SoundManager> sInstance;
	void PlaySound(Enum::Sound p_sound);
	void PlayJoinSound();
	void PlaySoundAtLocation(Enum::Sound p_sound, sf::Vector2f p_location);
	void PlayMusic();

protected:
	SoundManager();
	sf::Sound health, ammo, cloak, fire_rate, shotgun, armour, shoot, death, bark1, bark2, bark3, explosion1, explosion2;
	sf::SoundBuffer healthB, ammoB, cloakB, fire_rateB, shotgunB, armourB, pickupB, shootB, deathB, bark1B, bark2B, bark3B, explosion1B, explosion2B;
	sf::Music bgMusic;
	static void LoadSoundFromFile(sf::Sound& p_sound, sf::SoundBuffer& p_buffer, string p_file, bool is3D = false);
	static void LoadMusicFromFile(sf::Music& p_music, string p_file);
};
