#pragma once

class Grenade : public GameObject {
public:
	CLASS_IDENTIFICATION('GRDE', GameObject)

	static GameObject* StaticCreate()
	{
		return new Grenade();
	}

	uint32_t GetAllStateMask() const override
	{
		return GrenadeReplication::State::AllState;
	}

	void SetVelocity(const sf::Vector2f& inVelocity)
	{
		mVelocity = inVelocity;
	}

	const sf::Vector2f& GetVelocity() const
	{
		return mVelocity;
	}

	void SetPlayerId(int inPlayerId)
	{
		mPlayerId = inPlayerId;
	}

	uint32_t GetPlayerId() const
	{
		return mPlayerId;
	}

	void InitFromPlayer(Player* inPlayer, int variation);
	void Update() override;
	void HandleLocation();
	bool HandleCollisionWithPlayer(Player* inPlayer) override;
	bool HandleCollisionWithGameObject(GameObject* inObj) override;

	uint32_t Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const override;
	void SetThrowSpeed(float speed);

private:
	void WritePosition(OutputMemoryBitStream& inOutputStream, uint32_t& inDirtyState, uint32_t& writtenState) const;
	void WritePlayerID(OutputMemoryBitStream& inOutputStream, uint32_t& inDirtyState, uint32_t& writtenState) const;

protected:
	Grenade();
	
	sf::Vector2f mVelocity;
	float mThrowSpeed;
	uint32_t mPlayerId;

	bool mIsExploding;
};

typedef shared_ptr<Grenade> GrenadePtr;
