#pragma once
class Timer {
public:
	Timer(float finishTime);

	void Start();
	
	void Reset();
	void Update(float deltaTime);

	bool IsStarted() const;
	bool IsFinished() const;

	float GetElapsedSeconds() const;

private:
	void Tick(float deltaTime);
	void CheckFinished();
private:
	float mCurrentTime;
	float mFinishTime;

	bool mTimerStarted;
	bool mTimerFinished;
};
