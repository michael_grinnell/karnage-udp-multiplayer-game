#pragma once

namespace sf {
	class Sprite;
	class Text;
	class RectangleShape;
	class Time;
}

class Animation;

namespace Utility
{
	void centreOrigin(sf::RectangleShape shape);
	void centerOrigin(sf::Sprite& sprite);
	void centreOrigin(sf::Sprite& sprite);
	void centerOrigin(sf::Text& text);
	void centerOrigin(Animation& animation);
	void centreOrigin(Animation& animation);

	float toRadians(float degrees);
	float toDegrees(float radians);

	sf::Vector2f unitVector(sf::Vector2f vector);
	sf::Vector2f normalize(const sf::Vector2f& source);
	float length(sf::Vector2f vector);
	float vectorDistance(sf::Vector2f v1, sf::Vector2f v2);
	float NormalizedEuclideanDistance(sf::Vector2f v1, sf::Vector2f v2);
	float dot(const sf::Vector2f& inLeft, const sf::Vector2f& inRight);

	void clamp(float& value, float min, float max);
	void clamp(int& value, int min, int max);
	sf::Vector2f MoveTowards(sf::Vector2f current, sf::Vector2f target, float maxDelta);
	bool inRange(unsigned low, unsigned high, unsigned x);
	sf::Vector2f lerp(const sf::Vector2f& inA, const sf::Vector2f& inB, float t);

	int randomInt(int exclusiveMax);
	int randomRange(int range);
	sf::Vector2i GetRandomVector(const sf::Vector2i& inMax);
}



