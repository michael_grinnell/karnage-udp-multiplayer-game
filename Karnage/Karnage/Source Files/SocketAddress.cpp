#include "KarnagePCH.h"

SocketAddress::SocketAddress(uint32_t inAddress, uint16_t inPort) 
{
	GetAsSockAddrIn()->sin_family = AF_INET;
	GetIP4Ref() = htonl(inAddress);
	GetAsSockAddrIn()->sin_port = htons(inPort);
}

SocketAddress::SocketAddress(const sockaddr& inSockAddr)
{
	memcpy(&mSockAddr, &inSockAddr, sizeof(sockaddr));
}

SocketAddress::SocketAddress()
{
	GetAsSockAddrIn()->sin_family = AF_INET;
	GetIP4Ref() = INADDR_ANY;
	GetAsSockAddrIn()->sin_port = 0;
}

string SocketAddress::ToString() const
{
	#if _WIN32
	const sockaddr_in* s = GetAsSockAddrIn();
	char destinationBuffer[ 128 ];
	InetNtop(s->sin_family, const_cast<in_addr*>(&s->sin_addr), destinationBuffer, sizeof(destinationBuffer));
	return StringUtils::Sprintf("%s:%d", destinationBuffer, ntohs(s->sin_port));
	#else
	//not implement on mac for now...
	return string( "not implemented on mac for now" );
	#endif
}
