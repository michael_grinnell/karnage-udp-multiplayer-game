#include<KarnagePCH.h>

GameObject::GameObject() :
                         mColor(Colors::White)
                         , mCollisionRadius(1.f)
                         , mRotation(0.f)
                         , mScale(1.0f)
                         , mIndexInWorld(-1)
                         , mDoesWantToDie(false)
                         , mNetworkId(0) 
{
	/* Do Init */
}

void GameObject::Update()
{
	/* Object don't do anything by default. */
}

void GameObject::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
}

Vector3 GameObject::GetForwardVector() const
{
	/* Should we cache this when you turn? */
	return Vector3(sinf(mRotation), -cosf(mRotation), 0.f);
}

void GameObject::SetNetworkId(int inNetworkId)
{
	/* This doesn't put you in the map or remove you from it. */
	mNetworkId = inNetworkId;
}

void GameObject::SetRotation(int inRotation)
{
	/* Should we normalize using fmodf? */
	mRotation = inRotation;
}
