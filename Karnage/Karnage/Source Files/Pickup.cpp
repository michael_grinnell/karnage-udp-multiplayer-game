#include <KarnagePCH.h>

Pickup::Pickup()
{
	SetScale(GetScale() * .4f);
	SetCollisionRadius(20.f);
	picked = false;
}

bool Pickup::HandleCollisionWithPlayer(Player* inPlayer)
{
	(void)inPlayer;
	return false;
}

bool Pickup::HandleCollisionWithGameObject(GameObject* inObj)
{
	(void)inObj;
	return false;
}

void Pickup::SetPickupType(uint8_t type)
{
	mPickUpType = static_cast<Enum::PickupType>(type);
}

void Pickup::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
}

#pragma region Networking

uint32_t Pickup::Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const
{
	uint32_t writtenState = 0;

	WritePickupPosition(inOutputStream, writtenState, inDirtyState);
	WritePickupType(inOutputStream, writtenState, inDirtyState);

	return writtenState;
}

void Pickup::WritePickupPosition(OutputMemoryBitStream& inOutputStream, uint32_t& writtenState, uint32_t& inDirtyState) const
{
	if (inDirtyState & PickupReplication::State::Pos)
	{
		inOutputStream.Write(static_cast<bool>(true));
		sf::Vector2f location = GetLocation();
		inOutputStream.Write(location.x);
		inOutputStream.Write(location.y);
		inOutputStream.Write(GetRotation());
		writtenState |= PickupReplication::State::Pos;
	}
	else
	{
		inOutputStream.Write(static_cast<bool>(false));
	}
}

void Pickup::WritePickupType(OutputMemoryBitStream& inOutputStream, uint32_t& writtenState, uint32_t& inDirtyState) const
{
	if (inDirtyState & PickupReplication::State::Type)
	{
		inOutputStream.Write(static_cast<bool>(true));
		inOutputStream.Write(static_cast<uint8_t>(mPickUpType), Constant::Networking::PICKUP_TYPE_BITS); //TODO Smaller Data Type 
		writtenState |= PickupReplication::State::Type;
	}
	else
	{
		inOutputStream.Write(static_cast<bool>(false));
	}
}
#pragma endregion