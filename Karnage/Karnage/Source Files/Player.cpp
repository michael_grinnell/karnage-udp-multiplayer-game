#include <KarnagePCH.h>
#include <iostream>
//zoom hardcoded at 100...if we want to lock players on screen, this could be calculated from zoom
const float HALF_WORLD_HEIGHT = 3.6f;
const float HALF_WORLD_WIDTH = 6.4f;

Player::Player() :
	GameObject()
	, mVelocity(sf::Vector2f(0, 0))
	, mMaxRotationSpeed(5.f)
	, mWallRestitution(0.1f)
	, mPlayerRestitution(0.1f)
	, mPlayerId(0)
	, mThrustDir(sf::Vector2f(0.f, 0.f))
	, mHealth(Constant::Player::MAX_HEALTH)
	, mArmour(Constant::Player::MIN_ARMOUR)
	, mGrenadeAmmo(3)
	, mIsShooting(false)
	, mEquipedShotgun(false)
	, mIsThrowingGrenade(false)
	, mPickupType(Enum::PickupType::NONE)
	, mTimeBetweenShots(0.2f)
	, mHUD(new HUD(this))
	, mPlayerArrow(new PlayerArrow())
	, mIsLocal(false)
	, flash(false)
{
	SetCollisionRadius(60.f);
	UpdateHUDHealth();
	UpdateHUDArmour();
	mHUD->UpdateGrenadeAmmo();
}

#pragma region Getters & Setters

HUD* Player::GetHUD() const
{
	return mHUD;
}

PlayerArrow* Player::GetPlayerArrow() const
{
	return mPlayerArrow;
}

GameObject* Player::StaticCreate()
{
	return new Player();
}

uint32_t Player::GetAllStateMask() const
{
	return PlayerReplication::State::AllState;
}

Player* Player::GetAsPlayer()
{
	return this;
}

float Player::GetFireRate() const
{
	return mTimeBetweenShots;
}

void Player::SetFlash()
{
	flash = true;
}

void Player::SetLocal()
{
	mIsLocal = true;
}

void Player::SetPlayerId(uint32_t inPlayerId)
{
	mPlayerId = inPlayerId;
}

void Player::SetPlayerName(string name)
{
	mPlayerName = name;
}

uint32_t Player::GetPlayerId() const
{
	return mPlayerId;
}

string Player::GetPlayerName() const
{
	return mPlayerName;
}

void Player::SetVelocity(const sf::Vector2f& inVelocity)
{
	mVelocity = inVelocity;
}

const sf::Vector2f& Player::GetVelocity() const
{
	return mVelocity;
}

const Enum::PickupType& Player::GetPickupType() const
{
	return mPickupType;
}

uint8_t& Player::GetHealth()
{
	return mHealth;
}

uint8_t& Player::GetArmour()
{
	return mArmour;
}

uint8_t& Player::GetGrenadeAmmo()
{
	return mGrenadeAmmo;
}

void Player::SetGrenadeAmmo(uint8_t ammo)
{
	mGrenadeAmmo = ammo;
}

void Player::PickupHealth(uint8_t health)
{
	mHealth = ((health + mHealth) < Constant::Player::MAX_HEALTH) ? health + mHealth : Constant::Player::MAX_HEALTH;
}

void Player::PickupArmour(uint8_t armour)
{
	mArmour = ((armour + mArmour) < Constant::Player::MAX_ARMOUR) ? armour + mArmour : Constant::Player::MAX_ARMOUR;
}

void Player::SetPickupType(Enum::PickupType type)
{
	mPickupType = type;
}

void Player::SetReadyToDie(bool ready)
{
	mReadyToDie = ready;
}

bool Player::GetShotgun()
{ 
	return mEquipedShotgun;
}
#pragma endregion

#pragma region Get States

bool Player::IsMoving() const
{
	return mThrustDir != sf::Vector2i(0.f, 0.f);
}

bool Player::IsLocal() const
{
	return mIsLocal;
}

#pragma endregion

#pragma region Update

void Player::Update() { }

void Player::ProcessInput(float inDeltaTime, const InputState& inInputState)
{
	mThrustDir.x = inInputState.GetDesiredHorizontalDelta();
	mThrustDir.y = -inInputState.GetDesiredVerticalDelta();
	SetRotation(inInputState.GetRotation() + 90);
	mIsShooting = inInputState.IsShooting();

	mIsThrowingGrenade = inInputState.IsThrowingGrenade();
}

void Player::AdjustVelocityByThrust(float inDeltaTime)
{
	float x = mThrustDir.x * inDeltaTime * Constant::Player::SPEED;
	float y = mThrustDir.y * inDeltaTime * Constant::Player::SPEED;

	sf::Vector2f newVelocity = sf::Vector2f(x, y);

	if (x != 0 && y != 0)
		newVelocity /= std::sqrt(2.f);

	mVelocity = newVelocity;
}

void Player::SimulateMovement(float inDeltaTime)
{
	AdjustVelocityByThrust(inDeltaTime);
	SetLocation((GetLocation() + mVelocity));
	ProcessCollisions();
}

void Player::UpdateHUDAmmo() const
{
	mHUD->UpdateGrenadeAmmo();
}

void Player::UpdateHUDName() const
{
	mHUD->SetPlayerName();
	mPlayerArrow->setName(mPlayerName);
}

void Player::UpdateHUDHealth() const
{
	mHUD->UpdateHealthBar();
}
void Player::UpdateHUDArmour() const
{
	mHUD->UpdateArmourBar();
}

#pragma endregion

#pragma region Events

void Player::ApplyKickBack(float inDeltaTime)
{
	sf::Vector2f vel = GetVelocity();
	sf::Vector2f temp = sf::Vector2f(0, -1);
	thor::rotate(temp, GetRotation()); 
	////TODO This will need to account for collisions
	SetLocation(GetLocation() + (sf::Vector2f(temp.x * -Constant::Player::GUN_KICKBACK, temp.y * -Constant::Player::GUN_KICKBACK)));
}

void Player::ApplyPickup()
{
	switch (mPickupType) 
	{
	case Enum::PickupType::CLOAK:
		mIsCloaked = true;
		mEquipedShotgun = false;
		mTimeBetweenShots = Constant::Player::FIRE_RATE;
		break;

	case Enum::PickupType::FIRE_RATE:
		mIsCloaked = false;
		mEquipedShotgun = false;
		mTimeBetweenShots = Constant::Pickup::FIRE_RATE_MULTIPLIER;
		break;

	case Enum::PickupType::AMMO:
		mGrenadeAmmo = Constant::Pickup::PICKUP_AMMO;
		UpdateHUDAmmo();
		break;

	case Enum::PickupType::SHOTGUN:
		mIsCloaked = false;
		mTimeBetweenShots = mTimeBetweenShots = Constant::Player::FIRE_RATE;
		mEquipedShotgun = true;
		break;

	case Enum::PickupType::NONE:
		mIsCloaked = false;
		mTimeBetweenShots = Constant::Player::FIRE_RATE;
		mEquipedShotgun = false;
		break;

	default:
		break;
	}
}

#pragma endregion

#pragma region Collisions

void Player::ProcessCollisions()
{
	float sourceRadius = GetCollisionRadius();
	sf::Vector2f sourceLocation = GetLocation();
	
	/*
		Iterate through world to determine any hits.
		Due to a small number of objects, it's acceptable.
		Brute-force checking isn't an efficient solution to collisions.
		A solution would be to process collisions on objects themselves like in Multiplayer CA1/CA2.
	*/
	for (const auto& goIt : World::sInstance->GetGameObjects())
	{
		GameObject* target = goIt.get();

		/* Ignore Pickup Collisions */
		if (target->GetClassId() == 'PKUP') {}

		/* If target isn't player & isn't set to destroy */
		if (target != this && !target->DoesWantToDie())
		{
			//simple collision test for spheres- are the radii summed less than the distance?
			sf::Vector2f targetLocation = target->GetLocation();
			float targetRadius			= target->GetCollisionRadius();
			sf::Vector2f delta			= targetLocation - sourceLocation;
			float distSq				= Utility::dot(delta, delta);
			float collisionDist			= (sourceRadius + targetRadius);

			if (distSq < (collisionDist * collisionDist))
			{
				//first, tell the other guy there was a collision with a player, so it can do something...
				if (target->HandleCollisionWithPlayer(this))
				{
					/*
						Hit something.
						Project location far enough that you're not colliding.
					*/
					sf::Vector2f dirToTarget = Utility::normalize(delta);

					sf::Vector2f acceptableDeltaFromSourceToTarget = dirToTarget * collisionDist;

					/* To Note: We only move this player. The other player can take care of moving itself */
					SetLocation(targetLocation - acceptableDeltaFromSourceToTarget);
					sf::Vector2f relVel = mVelocity;

					/* If other object is a player, it might have velocity, so there might be relative velocity. */
					Player * targetPlayer = target->GetAsPlayer();

					if (targetPlayer)
					{
						/* Got VEL with DIR between objects to figure out if they're moving towards each other. */
						relVel -= targetPlayer->mVelocity;
					} 

					/* If so, the magnitude of the impulse ( since they're both just balls ) */
					float relVelDotDir = Utility::dot(relVel, dirToTarget);


					if (relVelDotDir > 0.f)
					{
						sf::Vector2f impulse = relVelDotDir * dirToTarget;
						if (targetPlayer)
						{
							mVelocity -= impulse;
							mVelocity *= mPlayerRestitution;
						}
						else
						{
							mVelocity -= impulse * 2.f;
							mVelocity *= mWallRestitution;
						}
					}
				}
			}
		}
	}
}

#pragma endregion

#pragma region Networking

uint32_t Player::Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const
{
	uint32_t writtenState = 0;

	WritePlayerID(inOutputStream, inDirtyState, writtenState);

	WritePlayerPosition(inOutputStream, inDirtyState, writtenState);

	WritePlayerThrust(inOutputStream, inDirtyState, writtenState);

	WritePlayerHealth(inOutputStream, inDirtyState, writtenState);

	WritePlayerArmour(inOutputStream, inDirtyState, writtenState);

	WritePlayerAmmo(inOutputStream, inDirtyState, writtenState);

	WritePlayerPickup(inOutputStream, inDirtyState, writtenState);

	return writtenState;
}

void Player::WritePlayerID(OutputMemoryBitStream& inOutputStream, uint32_t& inDirtyState, uint32_t& writtenState) const
{
	if (inDirtyState & PlayerReplication::State::PlayerId)
	{
		inOutputStream.Write(static_cast<bool>(true));
		inOutputStream.Write(GetPlayerId());

		//TODO Limit Player Name Size
		inOutputStream.Write(GetPlayerName());
		writtenState |= PlayerReplication::State::PlayerId;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}
}

void Player::WritePlayerPosition(OutputMemoryBitStream& inOutputStream, uint32_t& inDirtyState, uint32_t& writtenState) const
{
	if (inDirtyState & PlayerReplication::State::Pos)
	{
		inOutputStream.Write(static_cast<bool>(true));
		sf::Vector2f velocity = mVelocity;
		inOutputStream.Write(velocity.x);
		inOutputStream.Write(velocity.y);
		sf::Vector2f location = GetLocation();
		inOutputStream.Write(location.x);
		inOutputStream.Write(location.y);
		inOutputStream.Write(GetRotation());
		writtenState |= PlayerReplication::State::Pos;
	}
	else
	{
		inOutputStream.Write(static_cast<bool>(false));
	}
}

void Player::WritePlayerThrust(OutputMemoryBitStream& inOutputStream, uint32_t& inDirtyState, uint32_t& writtenState) const
{
	if (mThrustDir != sf::Vector2i(0, 0))
	{
		inOutputStream.Write(true);
		inOutputStream.Write(mThrustDir.x > 0);
		inOutputStream.Write(mThrustDir.y > 0);
	}
	else
	{
		inOutputStream.Write(false);
	}
}

void Player::WritePlayerHealth(OutputMemoryBitStream& inOutputStream, uint32_t& inDirtyState, uint32_t& writtenState) const
{
	if (inDirtyState & PlayerReplication::State::Health)
	{
		//TODO Number of Bits FOr Health & AMMO
		inOutputStream.Write(static_cast<bool>(true));
		inOutputStream.Write(mHealth, Constant::Networking::HEALTH_BITS);
		writtenState |= PlayerReplication::State::Health;
	}
	else
	{
		inOutputStream.Write(static_cast<bool>(false));
	}
}

void Player::WritePlayerAmmo(OutputMemoryBitStream& inOutputStream, uint32_t& inDirtyState, uint32_t& writtenState) const
{
	if (inDirtyState & PlayerReplication::State::Ammo)
	{
		inOutputStream.Write(static_cast<bool>(true));
		inOutputStream.Write(mGrenadeAmmo, Constant::Networking::AMMO_BITS);
		writtenState |= PlayerReplication::State::Ammo;
	}
	else
	{
		inOutputStream.Write(static_cast<bool>(false));
	}
}

void Player::WritePlayerPickup(OutputMemoryBitStream& inOutputStream, uint32_t& inDirtyState, uint32_t& writtenState) const
{
	if (inDirtyState & PlayerReplication::State::Pickup)
	{
		uint8_t type = static_cast<uint8_t>(mPickupType);
		inOutputStream.Write(static_cast<bool>(true));
		inOutputStream.Write(type, Constant::Networking::PICKUP_TYPE_BITS); //TODO Smaller Data Type
		writtenState |= PlayerReplication::State::Pickup;
	}
	else
	{
		inOutputStream.Write(static_cast<bool>(false));
	}
}

void Player::WritePlayerArmour(OutputMemoryBitStream& inOutputStream, uint32_t& inDirtyState, uint32_t& writtenState) const
{
	if (inDirtyState & PlayerReplication::State::Armour)
	{
		//TODO Number of Bits FOr Health & AMMO
		inOutputStream.Write(static_cast<bool>(true));
		inOutputStream.Write(mArmour, Constant::Networking::ARMOUR_BITS);
		writtenState |= PlayerReplication::State::Armour;
	}
	else
	{
		inOutputStream.Write(static_cast<bool>(false));
	}
}

#pragma endregion
