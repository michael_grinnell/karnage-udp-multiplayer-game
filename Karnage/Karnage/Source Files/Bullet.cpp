#include <KarnagePCH.h>

Bullet::Bullet() :
                 mVelocity(sf::Vector2f(0,0))
                 , mMuzzleSpeed(400.f)
                 , mPlayerId(0)
{
	SetScale(GetScale() * .02f);
	SetCollisionRadius(15.f);
}

uint32_t Bullet::Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const
{
	uint32_t writtenState = 0;
	if(inDirtyState & EYRS_Pose)
	{
		inOutputStream.Write((bool)true);
		sf::Vector2f location = GetLocation();
		inOutputStream.Write(location.x);
		inOutputStream.Write(location.y);
		sf::Vector2f velocity = GetVelocity();
		inOutputStream.Write(velocity.x);
		inOutputStream.Write(velocity.y);
		inOutputStream.Write(GetRotation());
		writtenState |= EYRS_Pose;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}

	if(inDirtyState & EYRS_PlayerId)
	{
		inOutputStream.Write((bool)true);
		inOutputStream.Write(mPlayerId, Constant::Networking::PLAYER_ID_BITS);
		writtenState |= EYRS_PlayerId;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}
	return writtenState;
}

bool Bullet::HandleCollisionWithPlayer(Player* inPlayer)
{
	(void)inPlayer; //you hit a player, so look like you hit a player
	return false;
}

bool Bullet::HandleCollisionWithGameObject(GameObject* inObj)
{
	(void)inObj;
	return false;
}

void Bullet::ProcessBulletCollisions()
{
	float sourceRadius = GetCollisionRadius();
	sf::Vector2f sourceLocation = GetLocation();

	/*
		Iterate through world to determine any hits.
		Due to a small number of objects, it's acceptable.
		Brute-force checking isn't an efficient solution to collisions.
		A solution would be to process collisions on objects themselves like in Multiplayer CA1/CA2.
	*/
	for (const auto& goIt : World::sInstance->GetGameObjects())
	{
		GameObject* target = goIt.get();

		/* If target isn't player & isn't set to destroy */
		if (target != this && !target->DoesWantToDie())
		{
			//simple collision test for spheres- are the radii summed less than the distance?
			sf::Vector2f targetLocation = target->GetLocation();
			float targetRadius = target->GetCollisionRadius();
			sf::Vector2f delta = targetLocation - sourceLocation;
			float distSq = Utility::dot(delta, delta);
			float collisionDist = (sourceRadius + targetRadius);

			if (distSq < (collisionDist * collisionDist))
			{
				//first, tell the other guy there was a collision with a player, so it can do something...
				if (target->HandleCollisionWithGameObject(this))
				{
					/* Hit something. */
				}
			}
		}
	}
}

void Bullet::InitFromShooter(Player* inShooter, int variation)
{
	SetPlayerId(inShooter->GetPlayerId());
	sf::Vector2f vel = inShooter->GetVelocity();
	sf::Vector2f temp = sf::Vector2f(0.f, -1.f);

	thor::rotate(temp, inShooter->GetRotation() + variation);

	SetVelocity(sf::Vector2f(temp.x, temp.y) * mMuzzleSpeed);
	SetLocation(inShooter->GetLocation() + sf::Vector2f(temp.x, temp.y));
	SetRotation(inShooter->GetRotation());
}

void Bullet::Update()
{
	float deltaTime = Timing::sInstance.GetDeltaTime();
	SetLocation(GetLocation() + mVelocity * deltaTime);
	ProcessBulletCollisions();
}