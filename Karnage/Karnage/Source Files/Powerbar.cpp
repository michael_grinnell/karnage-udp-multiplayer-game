#include <KarnagePCH.h>


Powerbar::Powerbar(sf::Vector2f dimensions) :
	mDimensions(dimensions)
{
	mOutline.setSize(mDimensions);
	mOutline.setOutlineColor(sf::Color::Black);
	mOutline.setFillColor(sf::Color::Transparent);
	mOutline.setOutlineThickness(5);

	mFill.setSize(mDimensions);
}

void Powerbar::setColor(sf::Color color)
{
	mFill.setFillColor(color);
}

void Powerbar::setOutlineThickness(float thickness)
{
	mOutline.setOutlineThickness(thickness);
}

void Powerbar::setPosition(sf::Vector2f pos)
{
	mFill.setPosition(pos);
	mOutline.setPosition(pos);
}

void Powerbar::setPower(int power)
{
	mFill.setSize(sf::Vector2f(power > 0 ? power  : 0, power > 0 ? mDimensions.y : 0));
}

sf::Vector2f Powerbar::GetDimensions() const
{
	return mDimensions;
}

float Powerbar::GetOutlineThinkness() const
{
	return mOutline.getOutlineThickness();
}
void Powerbar::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(mOutline, states);
	target.draw(mFill, states);
}
