#include "KarnagePCH.h"
std::unique_ptr<ConnectionDetails> ConnectionDetails::sInstance;

void ConnectionDetails::StaticInit()
{
	ConnectionDetails* cd = new ConnectionDetails();
	cd->Load();
	sInstance.reset(cd);
}

ConnectionDetails::ConnectionDetails():
                                      m_clientPort(0)
                                      , m_serverPort(0) {}

void ConnectionDetails::Load()
{
	uint16_t port = 50001;
	m_clientPort = port;
	m_serverPort = port;
	StringUtils::Log("TRYING TO OPEN FILES", 1);
	string line;
	char name[Constant::World::MAX_NAME_SIZE];
	std::ifstream nameFile("../Assets/inputs/name.txt");
	if(nameFile.good())
	{
		nameFile.get(name, Constant::World::MAX_NAME_SIZE);
		m_clientName = name;
	}
	nameFile.close();
	std::ifstream ipFile("../Assets/inputs/ip.txt");
	if(ipFile.good())
	{
		getline(ipFile, line);
		m_clientIP = line;
	}
	ipFile.close();
}

string ConnectionDetails::GetClientName() const
{
	return m_clientName;
}

string ConnectionDetails::GetClientDestination() const
{
	string asd = m_clientIP + ":" + std::to_string(m_clientPort);
	StringUtils::Log(asd.c_str(), 1);
	return m_clientIP + ":" + std::to_string(m_clientPort);
}

uint16_t ConnectionDetails::GetServerPort() const
{
	return m_serverPort;
}
