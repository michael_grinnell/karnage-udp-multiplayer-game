#include <KarnagePCH.h>

Obstacle::Obstacle()
{
	SetScale(GetScale() * .6f);
	SetCollisionRadius(45.f);
}

bool Obstacle::HandleCollisionWithPlayer(Player* inPlayer)
{
	(void)inPlayer;
	return false;
}

bool Obstacle::HandleCollisionWithGameObject(GameObject* inObj)
{
	(void)inObj;
	return false;
}

void Obstacle::SetObstacleType(uint8_t type)
{
	mObstacleType = static_cast<Enum::ObstacleType>(type);
}

void Obstacle::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
}

#pragma region Networking

uint32_t Obstacle::Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const
{
	uint32_t writtenState = 0;

	WriteObstaclePosition(inOutputStream, writtenState, inDirtyState);
	WriteObstacleType(inOutputStream, writtenState, inDirtyState);

	return writtenState;
}

void Obstacle::WriteObstaclePosition(OutputMemoryBitStream& inOutputStream, uint32_t& writtenState, uint32_t& inDirtyState) const
{
	if (inDirtyState & ObstacleReplication::State::Pos)
	{
		inOutputStream.Write(static_cast<bool>(true));
		sf::Vector2f location = GetLocation();
		inOutputStream.Write(location.x);
		inOutputStream.Write(location.y);
		inOutputStream.Write(GetRotation());
		writtenState |= ObstacleReplication::State::Pos;
	}
	else
	{
		inOutputStream.Write(static_cast<bool>(false));
	}
}

void Obstacle::WriteObstacleType(OutputMemoryBitStream& inOutputStream, uint32_t& writtenState, uint32_t& inDirtyState) const
{
	if (inDirtyState & ObstacleReplication::State::Type)
	{
		inOutputStream.Write(static_cast<bool>(true));
		inOutputStream.Write(static_cast<uint8_t>(mObstacleType), Constant::Networking::OBSTACLE_TYPE_BITS); //TODO Smaller Data Type 
		writtenState |= ObstacleReplication::State::Type;
	}
	else
	{
		inOutputStream.Write(static_cast<bool>(false));
	}
}
#pragma endregion
