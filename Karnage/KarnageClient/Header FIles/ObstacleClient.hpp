#pragma once
class ObstacleClient : public Obstacle {
public:
	static GameObjectPtr StaticCreate()
	{
		return GameObjectPtr(new ObstacleClient());
	}

	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	bool HandleCollisionWithGameObject(GameObject* inObj) override;

protected:
	ObstacleClient();
	void SetObstacleType(uint8_t type);
	void Read(InputMemoryBitStream& inInputStream) override;
	void ReadObstaclePosition(InputMemoryBitStream& inInputStream, bool& stateBit);
	void ReadObstacleType(InputMemoryBitStream& inInputStream, bool& stateBit);
	void SetTextureRect() const;
private:
	SFSpriteComponentPtr m_sprite;
};