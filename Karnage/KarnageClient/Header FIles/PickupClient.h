#pragma once
class PickupClient : public Pickup{
public:
	static GameObjectPtr StaticCreate()
	{
		return GameObjectPtr(new PickupClient());
	}

	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

protected:
	PickupClient();
	void SetPickupType(uint8_t type);
	void SetTextureRect() const;
	void Read(InputMemoryBitStream& inInputStream) override;
	void ReadPickupPosition(InputMemoryBitStream& inInputStream, bool& stateBit);
	void ReadPickupType(InputMemoryBitStream& inInputStream, bool& stateBit);
private:
	SFSpriteComponentPtr m_sprite;
};
