#pragma once
class BulletClient : public Bullet{
public:
	static GameObjectPtr StaticCreate()
	{
		return GameObjectPtr(new BulletClient());
	}

	void Read(InputMemoryBitStream& inInputStream) override;
	bool HandleCollisionWithPlayer(Player* inPlayer) override;
	bool HandleCollisionWithGameObject(GameObject* inObj) override;

	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

protected:
	BulletClient();
private:
	SFSpriteComponentPtr m_sprite;
};
