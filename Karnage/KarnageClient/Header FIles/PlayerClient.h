#pragma once
class PlayerClient : public Player{
public:
	static GameObjectPtr StaticCreate()
	{
		return GameObjectPtr(new PlayerClient());
	}

	void Update() override;
	
	void HandleDying() override;
	void Read(InputMemoryBitStream& inInputStream) override;

	void DoClientSidePredictionAfterReplicationForLocalPlayer(uint32_t inReadState);
	void DoClientSidePredictionAfterReplicationForRemotePlayer(uint32_t inReadState);

	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	virtual ~PlayerClient();

protected:
	PlayerClient();
	
private:
	void InterpolateClientSidePrediction(float inOldRotation, const sf::Vector2f& inOldLocation,
	                                     const sf::Vector2f& inOldVelocity, bool inIsForRemotePlayer);
	void SetAnimations();
	void UpdateAnimations();
	void UpdateHealth(uint8_t health);
	void UpdateArmour(uint8_t);
	void HandleFlashTimer();

	void StartFlashTimer();

	void FinishFlashTimer();

	void HandleLocalPlayerMovement();
	void HandleOtherPlayerMovement();
	void HandlePrediction(uint32_t& readState);
	void HandlePickup(Enum::PickupType type);

	void ReadPlayerID(InputMemoryBitStream& inInputStream, bool& stateBit, uint32_t& readState);
	void ReadPlayerPosition(InputMemoryBitStream& inInputStream, bool& stateBit, uint32_t& readState);
	void ReadPlayerThrust(InputMemoryBitStream& inInputStream, bool& stateBit, uint32_t& readState);
	void ReadPlayerHealth(InputMemoryBitStream& inInputStream, bool& stateBit, uint32_t& readState);
	void ReadPlayerAmmo(InputMemoryBitStream& inInputStream, bool& stateBit, uint32_t& readState);
	void ReadPlayerPickup(InputMemoryBitStream& inInputStream, bool& stateBit, uint32_t& readState);
	void ReadPlayerArmour(InputMemoryBitStream& inInputStream, bool& stateBit, uint32_t& readState);

private:
	sf::Vector2f mPlayerInfo;
	sf::Time m_gameTime;

	float mTimeLocationBecameOutOfSync;
	float mTimeVelocityBecameOutOfSync; 
	
	SFSpriteComponentPtr m_sprite;
	
	//TODO MAKE THESE POINTERS
	Animation mMoveAnimation;
	Animation mDeathAnimation;
};
