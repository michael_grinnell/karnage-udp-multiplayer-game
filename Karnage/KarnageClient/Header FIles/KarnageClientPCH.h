#pragma once
#include <KarnageShared.h>
#include <windows.h>
#include <SDL.h>
#include "SFML/Graphics.hpp"
#include <Log.h>
#include <InputManager.h>
#include <sstream>
#include <math.h>

#include <SFSpriteComponent.h>

#include "TileFactory.hpp"
#include <SFRenderManager.h>
#include <SFTextureManager.h>
#include <SFWindowManager.h>
#include "FontManager.h"
#include <PlayerClient.h>
#include <PickupClient.h>
#include <BulletClient.h>
#include <GrenadeClient.hpp>
#include "ObstacleClient.hpp"
#include "BloodstainClient.hpp"
//#include <SoundManager.h>
#include <ReplicationManagerClient.h>
#include <NetworkManagerClient.h>
#include <Client.h>
