#pragma once
class BloodstainClient : public Bloodstain {
public:
	static GameObjectPtr StaticCreate()
	{
		return GameObjectPtr(new BloodstainClient());
	}

	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

protected:
	BloodstainClient();
	void SetBloodstainType(uint8_t type);
	void Read(InputMemoryBitStream& inInputStream) override;
	void ReadBloodstainPosition(InputMemoryBitStream& inInputStream, bool& stateBit);
	void ReadBloodstainType(InputMemoryBitStream& inInputStream, bool& stateBit);
	void BloodstainClient::SetTextureRect() const;
private:
	SFSpriteComponentPtr m_sprite;
};