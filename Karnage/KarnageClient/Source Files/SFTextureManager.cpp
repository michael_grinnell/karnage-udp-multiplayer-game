#include <KarnageClientPCH.h>
std::unique_ptr<SFTextureManager> SFTextureManager::sInstance;

void SFTextureManager::StaticInit()
{
	sInstance.reset(new SFTextureManager());
}

SFTextureManager::SFTextureManager()
{
	//SpriteSheets
	CacheTexture("pickups", "../Assets/SpriteSheets/Pickups.png");
	CacheTexture("tileMap", "../Assets/SpriteSheets/Tiles.png");
	CacheTexture("playerMoveAnimation", "../Assets/SpriteSheets/PlayerMove.png"); 
	CacheTexture("playerDeathAnimation", "../Assets/SpriteSheets/blood.png");
	CacheTexture("explosion", "../Assets/SpriteSheets/Explosion.png");
	
	//Textures
	CacheTexture("bullet", "../Assets/Textures/bullet.png");//TODO CREATE SPRITESHEET
	CacheTexture("grenade", "../Assets/Textures/Grenade.png");
	CacheTexture("obstacles", "../Assets/Textures/Crate.png");

	CacheTexture("start_screen", "../Assets/Textures/start_screen.png");
	CacheTexture("died_screen", "../Assets/Textures/died_screen.png");
	CacheTexture("winner_screen", "../Assets/Textures/winner_screen.png"); // Healthbar frames.
}

SFTexturePtr SFTextureManager::GetTexture(const string& inTextureName)
{
	return mNameToTextureMap[inTextureName];
}

bool SFTextureManager::CacheTexture(string inName, const char* inFileName)
{
	SFTexturePtr newTex(new sf::Texture());
	if(!newTex->loadFromFile(inFileName))
		return false;
	mNameToTextureMap[inName] = newTex;
	return true;
}