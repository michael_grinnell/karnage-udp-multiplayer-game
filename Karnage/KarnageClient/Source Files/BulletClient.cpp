#include <KarnageClientPCH.h>

BulletClient::BulletClient()
{
	m_sprite.reset(new SFSpriteComponent(this));
	m_sprite->SetTexture(SFTextureManager::sInstance->GetTexture("bullet"));
}

void BulletClient::Read(InputMemoryBitStream& inInputStream)
{
	bool stateBit;
	inInputStream.Read(stateBit);
	if(stateBit)
	{
		sf::Vector2f location;
		inInputStream.Read(location.x);
		inInputStream.Read(location.y);
		sf::Vector2f velocity;
		inInputStream.Read(velocity.x);
		inInputStream.Read(velocity.y);
		SetVelocity(velocity); //dead reckon ahead by rtt, since this was spawned a while ago!
		SetLocation(location + velocity * NetworkManagerClient::sInstance->GetRoundTripTime());
		float rotation;
		inInputStream.Read(rotation);
		SetRotation(rotation);
	}

	inInputStream.Read(stateBit);
	if(stateBit)
	{
		inInputStream.Read(mPlayerId, Constant::Networking::PLAYER_ID_BITS);
		SoundManager::sInstance->PlaySoundAtLocation(Enum::Sound::SHOOT, GetLocation());
	}
} //you look like you hit a player on the client, so disappear ( whether server registered or not

bool BulletClient::HandleCollisionWithPlayer(Player* inPlayer)
{
	if(GetPlayerId() != inPlayer->GetPlayerId())
	{
		// Could do a SFML version here if needed.
	}
	return false;
}

bool BulletClient::HandleCollisionWithGameObject(GameObject* inObj)
{
	/* Do Object Collision Response */
	return false;
}

void BulletClient::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(m_sprite->GetSprite(), states);
}