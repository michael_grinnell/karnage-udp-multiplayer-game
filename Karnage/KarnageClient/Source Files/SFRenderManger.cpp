#include <KarnageClientPCH.h>

std::unique_ptr<SFRenderManager> SFRenderManager::sInstance;

#pragma region Initialization

SFRenderManager::SFRenderManager():
                                  mLocalPlayer(nullptr)
                                  , mRenderState()
                                  , mAlivePlayers(0)
                                  , mFrame(0)
                                  , mFPS(0)
                                  , mDrawnObjects(0)
                                  , mDebug(false)
								  , mLocalPlayerAlive(false)
								  , mLocalPlayerID(0)
								  , mGameReady(false)
								  , mGameStarted(false)
								  , mGameOver(false)
{}
		
void SFRenderManager::StaticInit()
{
	SFRenderManager* renderManager = new SFRenderManager();
	renderManager->Load();
	sInstance.reset(renderManager);
}

void SFRenderManager::Load()
{
	//DEBUG MODE VARIABLE
	mDebug = false;

	view.reset(sf::FloatRect(0, 0, Constant::Window::VIEW_WIDTH, Constant::Window::VIEW_HEIGHT));
	HUD.reset(sf::FloatRect(0, 0, Constant::Window::VIEW_WIDTH, Constant::Window::VIEW_HEIGHT));

	SFWindowManager::sInstance->setView(view);
	SetTextures();

	mTileFactory = TileFactory();
}

void SFRenderManager::SetTextures()
{
	m_startScreen.setTexture(*SFTextureManager::sInstance->GetTexture("start_screen"));
	m_diedScreen.setTexture(*SFTextureManager::sInstance->GetTexture("died_screen"));
	m_winnerScreen.setTexture(*SFTextureManager::sInstance->GetTexture("winner_screen"));
}

#pragma endregion

#pragma region Getters & Setters

sf::Vector2f SFRenderManager::FindPlayerCentre() const
{
	return mLocalPlayer != nullptr ? mLocalPlayer->GetLocation() : sf::Vector2f(-1, -1);
}

void SFRenderManager::DestroyLocalPlayer(uint32_t playerID)
{
	if (mLocalPlayerID == playerID)
		mLocalPlayer = nullptr;
}

uint8_t SFRenderManager::FindPlayerHealth() const
{
	return mLocalPlayer != nullptr ? mLocalPlayer->GetHealth() : 0;
}

void SFRenderManager::SetLocalPlayer(uint32_t id)
{
	std::vector<GameObjectPtr> gameObjects = World::sInstance->GetGameObjects();

	auto it = find_if(gameObjects.begin(), gameObjects.end(), [&id](const GameObjectPtr & go)
		{return (go->GetClassId() == Constant::GameObject::PLAYER) && (go->GetAsPlayer()->GetPlayerId() == id); });

	if (it != gameObjects.end())
	{
		mGameStarted = true;
		mLocalPlayerAlive = true;
		mLocalPlayer = dynamic_cast<Player*>(it->get());
		mLocalPlayer->SetLocal();
		mLocalPlayerID = mLocalPlayer->GetPlayerId();
	}
	else
		mLocalPlayer = nullptr;
}

void SFRenderManager::SetAlivePlayers()
{
	std::vector<GameObjectPtr> gameObjects = World::sInstance->GetGameObjects();

	mAlivePlayers = count_if(gameObjects.begin(), gameObjects.end(), [](const GameObjectPtr & go)
		{return (go->GetClassId() == Constant::GameObject::PLAYER) && (go->GetAsPlayer()->GetHealth() > 0); });
}

void SFRenderManager::SetRenderState()
{
	if (!GameStarted())
		mRenderState = START_SCREEN;
	else if (!LocalPlayerAlive())
		mRenderState = DEATH_SCREEN;
	else if (IsLastManStanding())
		mRenderState = WINNER_SCREEN;
	else
		mRenderState = GAME_SCREEN;	
}

bool SFRenderManager::GameStarted() const
{
	return (mGameReady && mGameStarted);
}

bool SFRenderManager::LocalPlayerAlive()
{
	if (mLocalPlayer)
	{
		if (mLocalPlayer->GetHealth() <= 0)
			mLocalPlayer = nullptr;
	}

	return mLocalPlayer;
}

bool SFRenderManager::IsLastManStanding() const
{

	return (ScoreBoardManager::sInstance->GetEntry(NetworkManagerClient::sInstance->GetPlayerId())->GetScore() >= Constant::Score::WINNING_SCORE_AMOUNT);
}

#pragma endregion

#pragma region Update

void SFRenderManager::Update()
{
	SetRenderState();
	UpdateLocalPlayer();
	UpdateView();
	SetAlivePlayers();
}

void SFRenderManager::UpdateLocalPlayer()
{
	if(mLocalPlayer)
		sf::Listener::setPosition(mLocalPlayer->GetLocation().x, mLocalPlayer->GetLocation().y, 50);
	else
		SetLocalPlayer(NetworkManagerClient::sInstance->GetPlayerId());
}

void SFRenderManager::RenderUI() const
{
	if (mDebug && mLocalPlayer)
	{
		float rttMS = NetworkManagerClient::sInstance->GetAvgRoundTripTime().GetValue() * 1000.f;

		string bandwidth = StringUtils::Sprintf("In %d  Bps, Out %d Bps",
			static_cast<int>(NetworkManagerClient::sInstance
				->GetBytesReceivedPerSecond().GetValue()),
			static_cast<int>(NetworkManagerClient::sInstance
				->GetBytesSentPerSecond().GetValue()));
		mLocalPlayer->GetHUD()->UpdateDebugInfo(rttMS, bandwidth, mDrawnObjects, mFPS);
		mLocalPlayer->GetHUD()->UpdatePlayerArrow(mArrowPosition);
		mLocalPlayer->GetHUD()->UpdateScoreBoard();
		mLocalPlayer->GetHUD()->UpdateScore();

		SFWindowManager::sInstance->draw(*mLocalPlayer->GetHUD());
	}
	else if (mLocalPlayer)
	{
		mLocalPlayer->GetHUD()->UpdatePlayerArrow(mArrowPosition);
		SFWindowManager::sInstance->draw(*mLocalPlayer->GetHUD());
		mLocalPlayer->GetHUD()->UpdateScore();
	}

}

void SFRenderManager::UpdateView()
{
	sf::Vector2f playerCenter = FindPlayerCentre();
	mViewSize = view.getSize();
	mViewCenter = view.getCenter();

	mArrowPosition = sf::Vector2f(playerCenter - mViewCenter);

	if(playerCenter != sf::Vector2f(-1, -1))
		view.setCenter(mViewCenter + ((playerCenter - mViewCenter) * Constant::Window::CAMERA_FOLLOW_RATE));

	SFWindowManager::sInstance->setView(view);
}

void SFRenderManager::CountFPS()
{
	if (mFrameClock.getElapsedTime().asSeconds() >= 1.f)
	{
		mFPS = mFrame;
		mFrame = 0;
		mFrameClock.restart();
	}
	++mFrame;
}

#pragma endregion

#pragma region Draw

void SFRenderManager::Render()
{
	if (mDebug)
		CountFPS();

	SFWindowManager::sInstance->clear(sf::Color::Color(200, 0, 0, 255));

	if (!mGameOver)
		Update();
	
	switch (mRenderState)
	{
	case SFRenderManager::START_SCREEN:
		mGameReady = true;
		SFWindowManager::sInstance->draw(m_startScreen);
		break;

	case SFRenderManager::WINNER_SCREEN:
		m_winnerScreen.setPosition(sf::Vector2f(mViewCenter.x - mViewSize.x / 2, mViewCenter.y - mViewSize.y / 2));
		SFWindowManager::sInstance->draw(m_winnerScreen);
		break;

	case SFRenderManager::DEATH_SCREEN:
		m_diedScreen.setPosition(sf::Vector2f(mViewCenter.x - mViewSize.x / 2, mViewCenter.y - mViewSize.y / 2));
		SFWindowManager::sInstance->draw(m_diedScreen);
		break;

	case SFRenderManager::GAME_SCREEN:
		DrawWorld();
		SFWindowManager::sInstance->setView(HUD);
		sInstance->RenderUI();
		break;
	default:
		break;
	}
	SFWindowManager::sInstance->setView(view);
	SFWindowManager::sInstance->display();
}

void SFRenderManager::DrawWorld()
{
	sInstance->RenderTexturedWorld();
	sInstance->RenderComponents();

}

void SFRenderManager::RenderComponents()
{
	////TODO Count OBJS ONLY for Debug
	mDrawnObjects = 0;
	sf::IntRect viewport =
		sf::IntRect(mViewCenter.x - mViewSize.x / 2, mViewCenter.y - mViewSize.y / 2, mViewSize.x, mViewSize.y);

	for (GameObjectPtr go : World::sInstance->GetGameObjects())
	{
		sf::Vector2f location = go->GetLocation();
		if (viewport.contains(sf::Vector2i(location.x, location.y)))
		{
			++mDrawnObjects;
			SFWindowManager::sInstance->draw(*go);
		}

	}

}

void SFRenderManager::RenderTexturedWorld() const
{
	sf::Texture map = mTileFactory.getTexture();
	SFWindowManager::sInstance->draw(mTileFactory.getVertices(), &map);
}
#pragma endregion