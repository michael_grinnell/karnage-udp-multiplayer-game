#include <KarnageClientPCH.h>

PlayerClient::PlayerClient() :
                             mTimeLocationBecameOutOfSync(0.f)
                             , mTimeVelocityBecameOutOfSync(0.f)
{
	m_sprite.reset(new SFSpriteComponent(this));
	SoundManager::sInstance->PlayMusic();
	SetAnimations();
	SoundManager::sInstance->PlaySound(Enum::Sound::JOIN);
	m_sprite->CenterOrigin();	
}

PlayerClient::~PlayerClient()
{
	uint32_t id = GetPlayerId();
	SFRenderManager::sInstance->DestroyLocalPlayer(id);
}

void PlayerClient::SetAnimations()
{
	mMoveAnimation.setTexture(*SFTextureManager::sInstance->GetTexture("playerMoveAnimation").get());
	mMoveAnimation.setFrameSize(Constant::Animation::PLAYER_MOVE_FRAME_SIZE);
	mMoveAnimation.setNumFrames(Constant::Animation::PLAYER_MOVE_FRAMES);
	mMoveAnimation.setDuration(Constant::Animation::PLAYER_MOVE_DURATION);
	mMoveAnimation.setRepeating(true);
	mMoveAnimation.setTextureRect();
	mMoveAnimation.CenterOrigin();

	mDeathAnimation.setTexture(*SFTextureManager::sInstance->GetTexture("playerDeathAnimation").get());
	mDeathAnimation.setFrameSize(Constant::Animation::PLAYER_DEATH_FRAME_SIZE);
	mDeathAnimation.setNumFrames(Constant::Animation::PLAYER_DEATH_FRAMES);
	mDeathAnimation.setDuration(Constant::Animation::PLAYER_DEATH_DURATION);
	mDeathAnimation.setTextureRect();
	mDeathAnimation.CenterOrigin();


	m_sprite->SetAnimation(mMoveAnimation);
}

#pragma region Update

void PlayerClient::Update()
{
	UpdateAnimations();
	mPlayerArrow->setPosition(GetLocation());
}

void PlayerClient::UpdateAnimations()
{
	if (mThrustDir != sf::Vector2i(0, 0) || mReadyToDie)
		m_sprite->UpdateAnimation(Timing::sInstance.GetDeltaTime());

	if (flash)
		HandleFlashTimer();
}

void PlayerClient::UpdateHealth(uint8_t health)
{
	if (health < mHealth)
		flash = true;
	else
		SoundManager::sInstance->PlaySound(Enum::Sound::P_HEALTH);

	if (Utility::inRange(0, Constant::Player::MAX_HEALTH, health))
		mHealth = health;
	
	if (mHealth <= 0)
	{
		SoundManager::sInstance->PlaySoundAtLocation(Enum::Sound::DEATH, GetLocation());
		mReadyToDie = true;
		m_sprite->SetAnimation(mDeathAnimation);
	}
	
	UpdateHUDHealth();
}

void PlayerClient::UpdateArmour(uint8_t armour)
{
	if (armour > mArmour)
		SoundManager::sInstance->PlaySound(Enum::Sound::P_ARMOUR);

	if (Utility::inRange(0, Constant::Player::MAX_ARMOUR, armour))
		mArmour = armour;


	UpdateHUDArmour();
}

#pragma endregion

#pragma region Events		

void PlayerClient::HandleLocalPlayerMovement()
{
	const Move* pendingMove = InputManager::sInstance->GetAndClearPendingMove();

	if (pendingMove)
	{
		float deltaTime = pendingMove->GetDeltaTime();         
		ProcessInput(deltaTime, pendingMove->GetInputState()); 
		SimulateMovement(deltaTime);                           
	}
}

void PlayerClient::HandleOtherPlayerMovement()
{
	SimulateMovement(Timing::sInstance.GetDeltaTime());
	if (GetVelocity() == sf::Vector2f(0, 0))
	{
		//we're in sync if our velocity is 0
		mTimeLocationBecameOutOfSync = 0.f;
	}
}

void PlayerClient::HandlePickup(Enum::PickupType type)
{
	mPickupType = type;
	ApplyPickup();
	switch (mPickupType)
	{
	case Enum::PickupType::CLOAK:
		mIsCloaked = true;
		m_sprite->SetColor(sf::Color(sf::Color(255,255,255,50)));
		SoundManager::sInstance->PlaySound(Enum::Sound::P_CLOAK);
		break;

	case Enum::PickupType::FIRE_RATE:
		mIsCloaked = false;
		m_sprite->SetColor(sf::Color(sf::Color::White));
		SoundManager::sInstance->PlaySound(Enum::Sound::P_FIRE_RATE);
		break;
	case Enum::PickupType::SHOTGUN:
		mIsCloaked = false;
		m_sprite->SetColor(sf::Color(sf::Color::White));
		SoundManager::sInstance->PlaySound(Enum::Sound::P_SHOTGUN);
		break;
	case Enum::PickupType::AMMO:
		SoundManager::sInstance->PlaySound(Enum::Sound::P_AMMO);
		break;

	case Enum::PickupType::NONE:
		mIsCloaked = false;
		m_sprite->SetColor(sf::Color(sf::Color::White));
		break;

	default:
		break;
	}
				

}

void PlayerClient::HandleDying()
{
	Player::HandleDying(); 
}

void PlayerClient::HandleFlashTimer()
{
	if (mFlashTimerStarted)
		FinishFlashTimer();
	else
		StartFlashTimer();
}

void PlayerClient::StartFlashTimer()
{
	m_sprite->SetColor(sf::Color(255, 33, 33));

	mFlashTimerStarted = true;
	mFlashTimer = 0;
}

void PlayerClient::FinishFlashTimer()
{
	float time = Timing::sInstance.GetDeltaTime();
	mFlashTimer += time;
	if (mFlashTimer > 0.25f)
	{
		m_sprite->SetColor(sf::Color::White);
		mFlashTimerStarted = false;
		flash = false;
	}
}

#pragma endregion

#pragma region Networking

void PlayerClient::Read(InputMemoryBitStream& inInputStream)
{
	bool stateBit;
	uint32_t readState = 0;

	ReadPlayerID(inInputStream, stateBit, readState);

	ReadPlayerPosition(inInputStream, stateBit, readState);

	ReadPlayerThrust(inInputStream, stateBit, readState);

	ReadPlayerHealth(inInputStream, stateBit, readState);

	ReadPlayerArmour(inInputStream, stateBit, readState);

	ReadPlayerAmmo(inInputStream, stateBit, readState);

	ReadPlayerPickup(inInputStream, stateBit, readState);

	HandlePrediction(readState);

}

void PlayerClient::ReadPlayerID(InputMemoryBitStream& inInputStream, bool& stateBit, uint32_t& readState)
{
	inInputStream.Read(stateBit);
	if (stateBit)
	{
		uint32_t playerId;
		inInputStream.Read(playerId);
		SetPlayerId(playerId);

		string name;
		inInputStream.Read(name);
		SetPlayerName(name);

		UpdateHUDName();

		//m_sprite->SetAsPlayer(playerId, name);
		

		readState |= PlayerReplication::State::PlayerId;
	}
}

void PlayerClient::ReadPlayerPosition(InputMemoryBitStream& inInputStream, bool& stateBit, uint32_t& readState)
{
	oldRotation = GetRotation();
	oldLocation = GetLocation();
	oldVelocity = GetVelocity();

	inInputStream.Read(stateBit);
	if (stateBit)
	{
		sf::Vector2f replicatedVelocity;
		inInputStream.Read(replicatedVelocity.x);
		inInputStream.Read(replicatedVelocity.y);
		SetVelocity(replicatedVelocity);

		sf::Vector2f replicatedLocation;
		inInputStream.Read(replicatedLocation.x);
		inInputStream.Read(replicatedLocation.y);
		SetLocation(replicatedLocation);

		float replicatedRotation;
		inInputStream.Read(replicatedRotation);
		SetRotation(replicatedRotation);

		readState |= PlayerReplication::State::Pos;
	}
}

void PlayerClient::ReadPlayerThrust(InputMemoryBitStream& inInputStream, bool& stateBit, uint32_t& readState)
{
	inInputStream.Read(stateBit);
	if (stateBit)
	{
		inInputStream.Read(stateBit);
		mThrustDir.x = stateBit ? 1 : -1;
		inInputStream.Read(stateBit);
		mThrustDir.y = stateBit ? 1 : -1;
	}
	else
	{
		mThrustDir.x = 0;
		mThrustDir.y = 0;
	}
}

void PlayerClient::ReadPlayerHealth(InputMemoryBitStream& inInputStream, bool& stateBit, uint32_t& readState)
{
	inInputStream.Read(stateBit);
	if (stateBit)
	{

		uint8_t health;
		inInputStream.Read(health, Constant::Networking::HEALTH_BITS);
		UpdateHealth(health);
		readState |= PlayerReplication::State::Health;
	}
}

void PlayerClient::ReadPlayerAmmo(InputMemoryBitStream& inInputStream, bool& stateBit, uint32_t& readState)
{
	inInputStream.Read(stateBit);
	if (stateBit)
	{
		inInputStream.Read(mGrenadeAmmo, Constant::Networking::AMMO_BITS);
		UpdateHUDAmmo();
		readState |= PlayerReplication::State::Ammo;
	}
}

void PlayerClient::ReadPlayerPickup(InputMemoryBitStream& inInputStream, bool& stateBit, uint32_t& readState)
{
	inInputStream.Read(stateBit);

	if (stateBit)
	{
			uint8_t type;
			inInputStream.Read(type, Constant::Networking::PICKUP_TYPE_BITS);
			HandlePickup(static_cast<Enum::PickupType>(type));
	}
}

void PlayerClient::ReadPlayerArmour(InputMemoryBitStream& inInputStream, bool& stateBit, uint32_t& readState)
{
	inInputStream.Read(stateBit);
	if (stateBit)
	{


		uint8_t armour;
		inInputStream.Read(armour, Constant::Networking::ARMOUR_BITS);
		UpdateArmour(armour);
		readState |= PlayerReplication::State::Armour;
		LOG("Armour: %d", armour);
	}
}

#pragma endregion

#pragma region Prediction

void PlayerClient::HandlePrediction(uint32_t& readState)
{
	if (GetPlayerId() == NetworkManagerClient::sInstance->GetPlayerId())
	{
		//did we get health? if so, tell the hud!
		if ((readState & PlayerReplication::State::Health) != 0)
		{
			//HUD::sInstance->SetPlayerHealth(mHealth);

		}
		DoClientSidePredictionAfterReplicationForLocalPlayer(readState); //if this is a create packet, don't interpolate
		if ((readState & PlayerReplication::State::PlayerId) == 0)
		{
			InterpolateClientSidePrediction(oldRotation, oldLocation, oldVelocity, false);
		}
	}
	else
	{
		DoClientSidePredictionAfterReplicationForRemotePlayer(readState);
		//will this smooth us out too? it'll interpolate us just 15% of the way there...
		if ((readState & PlayerReplication::State::PlayerId) == 0)
		{
			InterpolateClientSidePrediction(oldRotation, oldLocation, oldVelocity, true);
		}
	}
}

void PlayerClient::DoClientSidePredictionAfterReplicationForLocalPlayer(uint32_t inReadState)
{
	if ((inReadState & PlayerReplication::State::Pos) != 0)
	{
		//simulate pose only if we received new pose- might have just gotten thrustDir
		//in which case we don't need to replay moves because we haven't warped backwards
		//all processed moves have been removed, so all that are left are unprocessed moves
		//so we must apply them...
		const MoveList& moveList = InputManager::sInstance->GetMoveList();
		for (const Move& move : moveList)
		{
			float deltaTime = move.GetDeltaTime();
			ProcessInput(deltaTime, move.GetInputState());
			SimulateMovement(deltaTime);
		}
	}
}

void PlayerClient::InterpolateClientSidePrediction(float inOldRotation, const sf::Vector2f& inOldLocation,
	const sf::Vector2f& inOldVelocity, bool inIsForRemotePlayer)
{
	if (inOldRotation != GetRotation() && !inIsForRemotePlayer)
	{
		//LOG("ERROR! Move replay ended with incorrect rotation!", 0);
	}
	float roundTripTime = NetworkManagerClient::sInstance->GetRoundTripTime();
	if (inOldLocation != GetLocation())
	{
		//LOG( "ERROR! Move replay ended with incorrect location!", 0 );
		//have we been out of sync, or did we just become out of sync?
		float time = Timing::sInstance.GetFrameStartTime();
		if (mTimeLocationBecameOutOfSync == 0.f)
		{
			mTimeLocationBecameOutOfSync = time;
		}
		float durationOutOfSync = time - mTimeLocationBecameOutOfSync;
		if (durationOutOfSync < roundTripTime)
		{
			SetLocation(Utility::lerp(inOldLocation, GetLocation(),
				inIsForRemotePlayer ? (durationOutOfSync / roundTripTime) : 0.15f));
		}
	}
	else
	{
		//we're in sync
		mTimeLocationBecameOutOfSync = 0.f;
	}
	if (inOldVelocity != GetVelocity())
	{
		//LOG( "ERROR! Move replay ended with incorrect velocity!", 0 );
		//have we been out of sync, or did we just become out of sync?
		float time = Timing::sInstance.GetFrameStartTime();
		if (mTimeVelocityBecameOutOfSync == 0.f)
		{
			mTimeVelocityBecameOutOfSync = time;
		} //now interpolate to the correct value...
		float durationOutOfSync = time - mTimeVelocityBecameOutOfSync;
		if (durationOutOfSync < roundTripTime)
		{
			SetVelocity(Utility::lerp(inOldVelocity, GetVelocity(),
				inIsForRemotePlayer ? (durationOutOfSync / roundTripTime) : 0.15f));
		} //otherwise, fine...
	}
	else
	{
		//we're in sync
		mTimeVelocityBecameOutOfSync = 0.f;
	}
} //so what do we want to do here? need to do some kind of interpolation...

void PlayerClient::DoClientSidePredictionAfterReplicationForRemotePlayer(uint32_t inReadState)
{
	if ((inReadState & PlayerReplication::State::Pos) != 0)
	{
		//simulate movement for an additional RTT
		float rtt = NetworkManagerClient::sInstance->GetRoundTripTime();
		//LOG( "Other player came in, simulating for an extra %f", rtt );
		//let's break into framerate sized chunks though so that we don't run through walls and do crazy things...
		float deltaTime = 1.f / 30.f;
		while (true)
		{
			if (rtt < deltaTime)
			{
				SimulateMovement(rtt);
				break;
			}
			SimulateMovement(deltaTime);
			rtt -= deltaTime;
		}
	}
}

#pragma endregion

#pragma region Draw

void PlayerClient::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	m_sprite->CenterOrigin();

	target.draw(m_sprite->GetSprite(), states);

	if (!IsLocal() && !mIsCloaked)
		target.draw(*GetPlayerArrow(), states);

	/* Debug Collision Outline */
	//sf::FloatRect playerBoundingRect = m_sprite->GetSprite().getGlobalBounds();
	//sf::RectangleShape shape;
	//shape.setPosition(sf::Vector2f(playerBoundingRect.left, playerBoundingRect.top));
	//shape.setSize(sf::Vector2f(playerBoundingRect.width, playerBoundingRect.height));
	//shape.setFillColor(sf::Color::Transparent);
	//shape.setOutlineColor(sf::Color::Green);
	//shape.setOutlineThickness(1.f);
	//target.draw(shape, states);
}

#pragma endregion