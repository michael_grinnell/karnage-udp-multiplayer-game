#include <KarnageClientPCH.h>

void PickupClient::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(m_sprite->GetSprite(), states);
}

PickupClient::PickupClient()
{
	m_sprite.reset(new SFSpriteComponent(this));
	m_sprite->SetTexture(SFTextureManager::sInstance->GetTexture("pickups"));
}

void PickupClient::SetPickupType(uint8_t type)
{
	mPickUpType = static_cast<Enum::PickupType>(type);
	SetTextureRect();
	m_sprite->CenterOrigin();
}

void PickupClient::SetTextureRect() const
{
	m_sprite->CenterOrigin();
	switch(mPickUpType)
	{
	case Enum::PickupType::HEALTH:
			m_sprite->SetTextureRect(sf::IntRect(0, 0, 128, 128));
			break;
		case Enum::PickupType::CLOAK:
			m_sprite->SetTextureRect(sf::IntRect(128, 0, 128, 128));
			break;
		case Enum::PickupType::AMMO:
			m_sprite->SetTextureRect(sf::IntRect(256, 0, 128, 128));
			break;
		case Enum::PickupType::FIRE_RATE:
			m_sprite->SetTextureRect(sf::IntRect(384, 0, 128, 128));
			break;
		case Enum::PickupType::SHOTGUN:
			m_sprite->SetTextureRect(sf::IntRect(512, 0, 128, 128));
			break;
		case Enum::PickupType::ARMOUR:
			m_sprite->SetTextureRect(sf::IntRect(640, 0, 128, 128));
			break;
		default:
			break;
	}
}

#pragma region Networking

void PickupClient::Read(InputMemoryBitStream& inInputStream)
{
	bool stateBit;

	ReadPickupPosition(inInputStream, stateBit);

	ReadPickupType(inInputStream, stateBit);
}

void PickupClient::ReadPickupPosition(InputMemoryBitStream& inInputStream, bool& stateBit)
{
	inInputStream.Read(stateBit);
	if (stateBit)
	{
		sf::Vector2f location;
		inInputStream.Read(location.x);
		inInputStream.Read(location.y);
		SetLocation(location);
		float rotation;
		inInputStream.Read(rotation);
		SetRotation(rotation);
	}
}

void PickupClient::ReadPickupType(InputMemoryBitStream& inInputStream, bool& stateBit)
{
	inInputStream.Read(stateBit);
	if (stateBit)
	{
		uint8_t pickupType;
		inInputStream.Read(pickupType, Constant::Networking::PICKUP_TYPE_BITS);
		SetPickupType(pickupType);
	}
}

#pragma endregion