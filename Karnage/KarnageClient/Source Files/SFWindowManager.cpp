#include <KarnageClientPCH.h>
std::unique_ptr<sf::RenderWindow> SFWindowManager::sInstance;

bool SFWindowManager::StaticInit()
{
	sInstance.reset(new sf::RenderWindow(sf::VideoMode(Constant::Window::VIEW_WIDTH, Constant::Window::VIEW_HEIGHT), Constant::Window::WINDOW_TITLE));
	return true;
}