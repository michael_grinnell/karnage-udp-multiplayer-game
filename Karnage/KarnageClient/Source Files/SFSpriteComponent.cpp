#include <KarnageClientPCH.h>

SFSpriteComponent::SFSpriteComponent(GameObject* inGameObject) :
                                                               mGameObject(inGameObject)
{
	// Add to the render manager.
	m_sprite = new sf::Sprite;
}

SFSpriteComponent::~SFSpriteComponent()
{
}

void SFSpriteComponent::SetTexture(SFTexturePtr inTexture) const
{
	auto tSize = inTexture->getSize();
	m_sprite->setTexture(*inTexture);
	m_sprite->setOrigin(tSize.x / 2, tSize.y / 2);
	m_sprite->setScale(sf::Vector2f(1.f * mGameObject->GetScale(), 1.f * mGameObject->GetScale()));
}

void SFSpriteComponent::SetTextureRect(sf::IntRect textureRect) const
{
	m_sprite->setTextureRect(textureRect);
}

sf::Sprite& SFSpriteComponent::GetSprite()
{
	 //Update the sprite based on the game object stuff.
	auto pos = mGameObject->GetLocation();
	auto rot = mGameObject->GetRotation();
	m_sprite->setPosition(pos.x, pos.y);
	m_sprite->setRotation(rot);
	return *m_sprite;
}

void SFSpriteComponent::SetSprite(sf::Sprite& sprite)
{
	m_sprite = &sprite;
}

void SFSpriteComponent::SetAnimation(Animation& animation)
{
	mAnimation = animation;
	m_sprite = mAnimation.getSprite();
}

void SFSpriteComponent::UpdateAnimation(float dt)
{
	mAnimation.update(dt);
}

void SFSpriteComponent::SetColor(sf::Color color) const
{
	m_sprite->setColor(color);
}

void SFSpriteComponent::CenterOrigin() const
{
	sf::FloatRect bounds = m_sprite->getLocalBounds();
	m_sprite->setOrigin(std::floor(bounds.left + bounds.width / 2.f), std::floor(bounds.top + bounds.height / 2.f));
}

sf::Vector2f SFSpriteComponent::GetLocation() const
{
	return mGameObject->GetLocation();
}