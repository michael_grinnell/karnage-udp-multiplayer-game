#include <KarnageClientPCH.h>

bool Client::StaticInit()
{
	ConnectionDetails::StaticInit(); // Create the Client pointer first because it initializes SDL
	Client* client = new Client();
	InputManager::StaticInit(); // New Versions.

	// Window manager needs to be initialized before RenderManager.
	// TextureManager before TexturedWorld
	SFWindowManager::StaticInit();
	SFTextureManager::StaticInit();
	FontManager::StaticInit();
	SFRenderManager::StaticInit();
	SoundManager::StaticInit();
	sInstance.reset(client);
	return true;
}

Client::Client()
{
	GameObjectRegistry::sInstance->RegisterCreationFunction('PLYR', PlayerClient::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction('PKUP', PickupClient::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction('BULT', BulletClient::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction('GRDE', GrenadeClient::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction('OBST', ObstacleClient::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction('BLUD', BloodstainClient::StaticCreate);
	string destination = ConnectionDetails::sInstance->GetClientDestination();
	string name = ConnectionDetails::sInstance->GetClientName();
	SocketAddressPtr serverAddress = SocketAddressFactory::CreateIPv4FromString(destination);
	NetworkManagerClient::StaticInit(*serverAddress, name);
}

void Client::DoFrame()
{
	InputManager::sInstance->Update();
	Engine::DoFrame();
	NetworkManagerClient::sInstance->ProcessIncomingPackets();
	SFRenderManager::sInstance->Render();
	NetworkManagerClient::sInstance->SendOutgoingPackets();
}

void Client::HandleEvent(sf::Event& p_event)
{
	switch(p_event.type)
	{
		case sf::Event::KeyPressed:
			InputManager::sInstance->HandleInput(EIA_Pressed, p_event.key.code);
			break;
		case sf::Event::KeyReleased:
			InputManager::sInstance->HandleInput(EIA_Released, p_event.key.code);
			break;
		case sf::Event::MouseButtonPressed:
			InputManager::sInstance->HandleMouseInput(EIA_Pressed, p_event.key.code);
			break;
		case sf::Event::MouseButtonReleased:
			InputManager::sInstance->HandleMouseInput(EIA_Released, p_event.key.code);
			break;
		case sf::Event::MouseMoved:
			InputManager::sInstance->HandleMouseMoved();
			break;
		case sf::Event::Resized:
			InputManager::sInstance->SetViewSize();
			break;
		case sf::Event::JoystickButtonPressed:
			if(p_event.joystickButton.button == 0) // A on Joystick
			{
				auto shootingKey = sf::Keyboard::K;
				InputManager::sInstance->HandleInput(EIA_Pressed, shootingKey);
			}
			break;
		case sf::Event::JoystickButtonReleased:
			if(p_event.joystickButton.button == 0) // A on Joystick
			{
				auto shootingKey = sf::Keyboard::K;
				InputManager::sInstance->HandleInput(EIA_Released, shootingKey);
			}
			break;
		default:
			break;
	}
}

bool Client::PullEvent(sf::Event& p_event)
{
	return SFWindowManager::sInstance->pollEvent(p_event);
}