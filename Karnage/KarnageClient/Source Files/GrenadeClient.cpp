#include <KarnageClientPCH.h>

GrenadeClient::GrenadeClient()
{
	m_sprite.reset(new SFSpriteComponent(this));
	m_sprite->SetTexture(SFTextureManager::sInstance->GetTexture("grenade"));

	SetAnimations();
}

void GrenadeClient::SetAnimations()
{
	mExplosionAnimation.setTexture(*SFTextureManager::sInstance->GetTexture("explosion").get());
	mExplosionAnimation.setFrameSize(Constant::Animation::EXPLOSION_FRAME_SIZE);
	mExplosionAnimation.setNumFrames(Constant::Animation::EXPLOSION_FRAMES);
	mExplosionAnimation.setDuration(Constant::Animation::EXPLOSION_DURATION);
	mExplosionAnimation.setTextureRect();
	mExplosionAnimation.CenterOrigin();
}

void GrenadeClient::Update()
{
	if (mIsExploding)
	{
		m_sprite->UpdateAnimation(Timing::sInstance.GetDeltaTime());
	}
}

#pragma region Events

bool GrenadeClient::HandleCollisionWithPlayer(Player * inPlayer)
{
	if (GetPlayerId() != inPlayer->GetPlayerId())
	{

	}
	return false;
}

bool GrenadeClient::HandleCollisionWithGameObject(GameObject* inObj)
{
	/* Handle Collision Response */
	return false;
}

void GrenadeClient::Explode()
{
	mIsExploding = true;
	m_sprite->SetAnimation(mExplosionAnimation);
	SoundManager::sInstance->PlaySoundAtLocation(Enum::Sound::EXPLOSION, GetLocation());
}

#pragma endregion

#pragma region Networking		

void GrenadeClient::Read(InputMemoryBitStream& inInputStream)
{
	bool stateBit;

	ReadLocation(inInputStream, stateBit);

	ReadPlayerID(inInputStream, stateBit);

	ReadExploding(inInputStream, stateBit);

}

void GrenadeClient::ReadLocation(InputMemoryBitStream& inInputStream, bool& stateBit)
{
	inInputStream.Read(stateBit);
	if (stateBit)
	{
		sf::Vector2f location;
		inInputStream.Read(location.x);
		inInputStream.Read(location.y);
		sf::Vector2f velocity;
		inInputStream.Read(velocity.x);
		inInputStream.Read(velocity.y);
		SetVelocity(velocity);
		//dead reckon ahead by rtt, since this was spawned a while ago!
		SetLocation(location + velocity * NetworkManagerClient::sInstance->GetRoundTripTime());
		float rotation;
		inInputStream.Read(rotation);
		SetRotation(rotation);
	}
}

void GrenadeClient::ReadPlayerID(InputMemoryBitStream& inInputStream, bool& stateBit)
{
	inInputStream.Read(stateBit);
	if (stateBit)
	{
		inInputStream.Read(mPlayerId, Constant::Networking::PLAYER_ID_BITS);
	}
}

void GrenadeClient::ReadExploding(InputMemoryBitStream& inInputStream, bool& stateBit)
{
	inInputStream.Read(stateBit);

	if (stateBit)
		Explode();
}

void GrenadeClient::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(m_sprite->GetSprite(), states);
}

#pragma endregion