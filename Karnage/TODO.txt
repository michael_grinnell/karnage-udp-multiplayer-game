PRIORITY

- COMPRESSION OF DATA TYPES!!
- Win/Lose Screen Display Score

Final Things
- Uncomment Music
- Remove all commented out code - Do this at end
- Make Sure IT Runs In Build MODE!!!

IF YOU HAVE Time
- TIDY GRENADE CODE!!!
- get Debug Condition From txt File
- Reset Pickup Timer for 2nd Pickup


IMPROVEMENTS
- Dont need to send health for Pickups, Lava , Player hit, Just a bit to say what it was from
- Movement is not Smooth(Possibly caused By Time)
- Grenade Power Bar

IDEAS
- Wait for all players ready at start?
- Respawn, give players X lives? Winner is first to 10 or so
- Zombies??? - Probably Not, But the server could handle this
- Muzzle Flash
- Impact Effects?
- Blood Stains
- Obstacles!!!
- Red Screen when Hit
- Grenade, Different Damage depending on distance, Just do - X from center of object.
- Set Player Damage, Should be dif from lava damage
